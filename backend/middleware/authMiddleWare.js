const jwt = require('jsonwebtoken');
const {secret} = require('../config');
const User = require('../models/User');

module.exports = function(req, res, next) {
  if (req.method === 'OPTIONS') {
    next();
  }

  try {
    const token = req.headers.authorization.split(' ')[1];
    if (!token) {
      return res.status(400).json({
        message: 'The author is not authorized',
      });
    }

    const decodedData = jwt.verify(token, secret);
    req.user = decodedData;

    req.header = User.findById(decodedData.id).select('-password');

    next();
  } catch (error) {
    return res.status(400).json({
      message: 'ERROR',
    });
  }
};
