const express = require('express');
const PORT = 8080;
const mongoose = require('mongoose');
const authRouter = require('../backend/routes/authRouter');
const notesRouter = require('../backend/routes/notesRouter');
const swaggerUI = require('swagger-ui-express');
const swaggerJsDoc = require('swagger-jsdoc');
const YAML = require('yamljs');

const cors = require('cors');
const app = express();

const swaggerDocument = YAML.load('./openapi.yaml');

const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Library API',
      version: '1.0.0',
      description: 'A simple Express Library API',
    },
    servers: [
      {
        url: 'http://localhost:8080',
      },
    ],
  },
  apis: ['./routes/*.js'],
};
swaggerJsDoc(options);

app.use(express.json());
app.use(cors());
app.use('/api', authRouter, notesRouter);
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocument));

const start = async () => {
  try {
    await mongoose.connect(
        `mongodb+srv://Anastasiia:Yktx6aqu8pQgGpOY@cluster0.rktas.mongodb.net/myFirstDatabase?auth=true&w=majority`,
    );

    app.listen(PORT, () => {
      console.log(`server started on port ${PORT}`);
    });
    app.listen(80, function() {
      console.log('CORS-enabled web server listening on port 80');
    });
  } catch (error) {
    console.log(error);
  }
};
start();
