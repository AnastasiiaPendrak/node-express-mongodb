const {Schema, model} = require('mongoose');

const Note = new Schema({
  user_id: {
    type: String,
    required: true,
  },

  text: {type: String, required: true},
  createdDate: {type: String, required: true},
  completed: false,
});
module.exports = model('Note', Note);
