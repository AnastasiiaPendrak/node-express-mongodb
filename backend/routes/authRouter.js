// eslint-disable-next-line new-cap
const router = require('express').Router();
const {check} = require('express-validator');
const controller = require('../Controllers/authController');
const roleMiddleWare = require('../middleware/roleMiddleWare');

router.post('/auth/register', [
  check('username', 'This input can not be empty').notEmpty(),
  check('password', 'The password must contain 4-10 numbers')
      .isLength({min: 4, max: 10}),

], controller.registration);

router.post('/auth/login', controller.login);
router.get('/auth/users', roleMiddleWare(['USER']), controller.getUsers);
router.get('/users/me', controller.getUserProfile);
router.patch('/users/me', controller.updateProfile );
router.delete('/users/me', controller.deleteProfile );

module.exports = router;
