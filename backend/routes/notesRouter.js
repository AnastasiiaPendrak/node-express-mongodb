// eslint-disable-next-line new-cap
const router = require('express').Router();
const controller = require('../Controllers/notesController');

router.post('/notes', controller.postNote);
router.get('/notes', controller.getNotes);
router.get('/notes/:id', controller.getNoteById);
router.put('/notes/:id', controller.updateNote);
router.delete('/notes/:id', controller.deleteNote);
router.patch('/notes/:id', controller.completedNote);
module.exports = router;
