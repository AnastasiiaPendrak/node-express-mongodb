/* eslint-disable camelcase */
const Note = require('../models/Note');
const User = require('../models/User');

/**
 *
 *
 */
class notesController {
  /**
   *
   *@param {*} req
   *@param {*} res
   *@return {any}
   */
  async getNotes(req, res) {
    try {
      const user = await User.findOne(req.header._id);
      const user_id = user._id;
      const limit = req.body.limit;
      const offset = req.body.offset;

      const query = Note.find({user_id: user_id}).skip(offset).limit(limit);

      if (!user) {
        return res.status(400).json({
          message: 'Error',
        });
      }

      query.exec(function(err, notes) {
        if (err) {
          res.status(400).json(err);
          return;
        }

        res.status(200).json({
          offset: req.body.offset,
          limit: req.body.limit,
          count: notes.length,
          notes,
        });
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: 'Error',
      });
    }
  }

  /**
   *
   *@param {*} req
   *@param {*} res
   *@return {any}
   */
  async postNote(req, res) {
    try {
      const user = await User.findOne();
      const text = req.body.text;

      if (!user) {
        return res.status(400).json({
          message: 'Error',
        });
      }

      const newNote = new Note({
        text,
        user_id: user._id,
        username: user.username,
        completed: false,
        createdDate: user.createdAt,
      });
      await newNote.save();

      return res.status(200).json({
        message: 'Note successfully posted',
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: 'Error',
      });
    }
  }
  /**
   *
   *@param {*} req
   *@param {*} res
   *@return {any}
   */
  async updateNote(req, res) {
    try {
      const content = req.body.text;
      const note = await Note.findOne({_id: req.params.id});

      note.text = content;
      note.save();

      if (!content) {
        return res.status(400).json({
          message: 'Error',
        });
      }

      return res.status(200).json({
        message: 'Success',
      });
    } catch (error) {
      res.status(500).json({
        message: 'Error',
      });
    }
  }

  /**
   *
   *@param {*} req
   *@param {*} res
   *@return {any}
   */
  async getNoteById(req, res) {
    try {
      const note = await Note.findById(req.params.id);
      res.json(note);

      if (!note) {
        return res.status(400).json({
          message: 'Error',
        });
      }

      return res.status(200).json({
        message: 'Success',
      });
    } catch (error) {
      return res.status(500).json({
        message: 'Error',
      });
    }
  }

  /**
   *
   *@param {*} req
   *@param {*} res
   *@return {any}
   */
  async deleteNote(req, res) {
    try {
      const result = await Note.findByIdAndDelete(req.params.id);
      if (!result) {
        return res.status(400).json({
          message: 'Error',
        });
      }
      return res.status(200).json({
        message: 'Success',
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: 'Error',
      });
    }
  }

  /**
   *
   *@param {*} req
   *@param {*} res
   *@return {any}
   */
  async completedNote(req, res) {
    const note = await Note.findOne({_id: req.params.id});
    note.completed = true;
    note.save();

    try {
      if (!note) {
        return res.status(400).json({
          message: 'Error',
        });
      }

      return res.status(200).json({
        message: 'Success',
      });
    } catch (error) {
      return res.status(500).json({
        message: 'Error',
      });
    }
  }
}
// eslint-disable-next-line new-cap
module.exports = new notesController();
