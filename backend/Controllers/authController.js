const User = require('../models/User');
const Role = require('../models/Role');
const bcrypt = require('bcryptjs');
const {validationResult} = require('express-validator');
const jwt = require('jsonwebtoken');
const {secret} = require('../config');

const generateAccessToken = (id, roles) => {
  const payload = {id, roles};

  return jwt.sign(payload, secret, {
    expiresIn: '24h',
  });
};
/**
 *
 */
class authController {
  /**
   *@param {*} req
   *@param {*} res
   *@return {any}
   */
  async registration(req, res) {
    try {
      /**
       *
       */
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({
          message: 'Your password is not correct',
          errors,
        });
      }
      const {username, password} = req.body;
      const candidate = await User.findOne({username});
      if (candidate) {
        return res.status(400).json({
          message: 'Username already exists',
        });
      }
      const hashPassword = bcrypt.hashSync(password, 7);
      const userRole = await Role.findOne({value: 'USER'});

      const user = new User({
        username,
        password: hashPassword,
        roles: [userRole.value],
      });
      await user.save();

      return res.status(200).json({
        message: 'Success',
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: 'Error',
      });
    }
  }
  /**
   *@param {*} req
   *@param {*} res
   *@return {any}
   */
  async login(req, res) {
    try {
      const {username, password} = req.body;
      const user = await User.findOne({username});

      if (!user) {
        return res.status(400).json({
          message: `User with such username - ${username} is not exist`,
        });
      }

      const validPassword = bcrypt.compareSync(password, user.password);
      if (!validPassword) {
        return res.status(400).json({
          message: `Check your password`,
        });
      }

      const token = generateAccessToken(user._id, user.roles);
      return res.status(200).json({
        message: 'Success',
        jwt_token: token,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: 'Error',
      });
    }
  }
  /**
   *
   *@param {*} req
   *@param {*} res
   */
  async getUsers(req, res) {
    try {
      const users = await User.find();
      res.json(users);
    } catch (error) {
      console.log(error);
    }
  }
  /**
   *
   *@param {*} req
   *@param {*} res
   */
  async getUserProfile(req, res) {
    try {
      const users = await User.findOne(req.header._id);
      const user = {
        _id: users._id,
        username: users.username,
        createdDate: users.createdAt,
      };
      if (!users) {
        return res.status(400).json({
          message: 'User not found',
        });
      }

      if (users) {
        return res.status(200).json({
          user,
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: 'Error',
      });
    }
  }
  /**
   *@param {*} req
   *@param {*} res
   *@return {any}
   */
  async updateProfile(req, res) {
    const oldPassword = req.body.oldPassword;
    const newPassword = req.body.newPassword;
    const users = await User.findOne(req.header._id);
    const validPassword = bcrypt.compareSync(oldPassword, users.password);

    try {
      if (users) {
        if (!validPassword) {
          return res.status(400).json({
            message: `Check your password`,
          });
        }

        const hashPassword = bcrypt.hashSync(newPassword, 7);
        users.password = hashPassword;
        await users.save();

        return res.status(200).json({
          message: `Success`,
        });
      } else {
        return res.status(400).json({
          message: 'User not found',
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: 'Error',
      });
    }
  }
  /**
   *
   *@param {*} req
   *@param {*} res
   */
  async deleteProfile(req, res) {
    try {
      const users = await User.findOne(req.header._id);

      if (!users) {
        return res.status(400).json({
          message: 'Error',
        });
      }

      if (users) {
        await User.findByIdAndDelete(users._id);

        return res.status(200).json({
          message: 'Success',
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: 'Error',
      });
    }
  }
}
// eslint-disable-next-line new-cap
module.exports = new authController();
